import React, { createContext, useState }  from 'react';

export const AuthContext = createContext(null);

export default function AuthContextProvider(props) {
  const [authData, setAuthData] = useState({
    token: undefined,
    username: undefined,
    isLoggedIn: false,
  });

  return (
    <AuthContext.Provider value={{ authData, setAuthData }}>
      {props.children}
    </AuthContext.Provider>
  );
}