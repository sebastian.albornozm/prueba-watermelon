import * as React from "react";
import * as ReactDOM from "react-dom";
const Wrapper = React.lazy(() => import('./components/Wrapper'))

ReactDOM.render(
  <React.Suspense fallback={<div>...Cargando</div>}>
      <Wrapper />
  </React.Suspense>,
  document.getElementById('root')
);