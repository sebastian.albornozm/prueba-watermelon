import React from 'react';
import List from './List';
import SignIn from './SignIn';
import LogIn from './LogIn'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import AuthContextProvider from '../context/auth';

export default function Wrapper() {

  return (
    <Router>
      <AuthContextProvider>
        <Switch>
          <Route exact path='/signin' component={SignIn} />
          <Route exact path='/list' component={List} />
          <Route exact path='/login' component={LogIn} />
        </Switch>
      </AuthContextProvider>
    </Router>
  )
}

//          