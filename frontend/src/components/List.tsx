import React, { useEffect, useState, useContext } from 'react';
import Character from './Character'
import Axios from 'axios';
import * as material from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import { SERVER_URI } from './../constant/server_uri';

import { AuthContext } from '../context/auth';

const useStyles = material.makeStyles((theme: material.Theme) => material.createStyles({
  root: {
    flexGrow: 1,
    width: '80%',
    padding: 50,
    margin: '0 auto'
  },
  inline: {
    display: 'inline',
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: 'center',
    color: 'edf2f4',
  },
  pag: {
    display: 'flex',
    justifyContent: 'center',
    '& > *': {
      marginTop: theme.spacing(2),
      margin: '0 auto',
      padding: 50,
    },
  }
}));

export default function Personajes(props) {

  const clases = useStyles();

  const { authData, setAuthData } = useContext(AuthContext);

  const [characters, setCharacters] = useState({ personajes: [] });
  const [loading, setloading] = useState(true);
  const [page, setPage] = useState(1);

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  }

  let renderCharacters = characters.personajes.map((obj) => {
    return (
      <material.Grid item xs={4}>
        <Character
          img={obj.image}
          name={obj.name}
          status={obj.status}
          species={obj.species}
          gender={obj.gender}
        />
      </material.Grid>
    )
  })

  async function fetchChar() {
    const { data } = await Axios({
      method: 'get',
      url:  SERVER_URI +'repository/data',
      headers: { authorization: `Bearer ${authData.token}` },
      params: {
        page: page,
      }
    });
    setloading(false);
    setCharacters(data);
  }

  useEffect(() => {
    fetchChar();
  }, [page]);

  return (
    <div className={clases.root}>
      <h1 style={{ display: 'flex', justifyContent: 'center', color: "8d99ae" }}>{loading ? `` : `Every character in Rick and Morty series`}</h1>
      <material.Grid container spacing={1}>
        {loading ? `Loading...` : renderCharacters}
      </material.Grid>
      <Pagination count={34} page={page} onChange={handleChange} className={clases.pag} size="large" color="primary" />
    </div>
  )
}
