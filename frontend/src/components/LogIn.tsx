import React, { useContext, useState } from 'react';
import * as material from '@material-ui/core';
import Axios from 'axios';
import { AuthContext } from '../context/auth'
import { SERVER_URI } from '../constant/server_uri'; 

const useStyles = material.makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '50%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));


export default function SignIn(props) {

  const clases = useStyles();

  const { setAuthData } = useContext(AuthContext);

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  async function login(e) {
    e.preventDefault()
    //inicar sesion
    const { data } = await Axios({
      method: 'post',
      url: SERVER_URI + 'user/login',
      data: {
        username: username,
        password: password,
      },
    });
    if (data.token) {
      setAuthData({
        username,
        token: data.token,
        isLoggedIn: true,
      })
      props.history.push("/list");
    } else {
      props.history.push("/login");
    }
  };

  return (
    <material.Container component="main" maxWidth="xl">
      <material.CssBaseline />
      <video
        autoPlay
        muted
        loop
        style={{
          position: "absolute",
          width: "100%",
          left: "50%",
          top: "50%",
          height: "100%",
          objectFit: "cover",
          transform: "translate(-50%, -50%)",
          zIndex: -1,
        }}
      >
        <source src="https://media.giphy.com/media/S8kcDWOvua4l6lJ0Az/giphy.mp4" type="video/mp4"></source>
      </video>
      <div className={clases.paper}>
        <material.Typography component="h1" variant="h5">
          Inicia sesión
        </material.Typography>
        <form className={clases.form} noValidate>
          <material.TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Usuario"
            name="usuario"
            autoComplete="usuario"
            autoFocus
            onChange={(e) => { setUsername(e.target.value) }}
          >
          </material.TextField>
          <material.TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Contraseña"
            type="password"
            id="password"
            onChange={(e) => { setPassword(e.target.value) }}
          />
          <material.Button
            onClick={login}
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
            className={clases.submit}
          >
            Entrar
          </material.Button>
        </form>
      </div>
    </material.Container>
  )
}

