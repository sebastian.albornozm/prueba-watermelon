import React, { useState, useContext } from 'react';
import * as material from '@material-ui/core';
import Axios from 'axios';
import { AuthContext } from '../context/auth';
import { SERVER_URI } from '../constant/server_uri';


const useStyles = material.makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '50%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn(props) {

  const clases = useStyles();

  const { setAuthData } = useContext(AuthContext);

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  async function signup(e) {
    e.preventDefault();
    await Axios({
      method: 'post',
      url: SERVER_URI + '/user/signup',
      data: {
        username: username,
        password: password,
      }
    }).then(response => {
      if (response.data.token) {
        setAuthData({
          username,
          token: response.data.token,
          isLoggedIn: true,
        })
        props.history.push("/list");
      } else {
        props.history.push("/signin");
      }
    }).catch(error => {
      console.log(error);
    });      
  }

  return (
    <material.Container component="main" maxWidth="xl">
      <material.CssBaseline />
      <video
        autoPlay
        muted
        loop
        style={{
          position: "absolute",
          width: "100%",
          left: "50%",
          top: "50%",
          height: "100%",
          objectFit: "cover",
          transform: "translate(-50%, -50%)",
          zIndex: -1,
        }}
      >
        <source src="https://media.giphy.com/media/cjDax1AZDFWO7S60MM/giphy.mp4" type="video/mp4"></source>
      </video>
      <div className={clases.paper}>
        <material.Typography component="h1" variant="h5" gutterBottom style={{color: 'white'}}>
          Registrate
        </material.Typography>
        <form className={clases.form} noValidate>
          <material.TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email"
          label="Usuario"
          name="usuario"
          autoComplete="usuario"
          autoFocus
          onChange={(e) => {setUsername(e.target.value)}}
          >
          </material.TextField>
          <material.TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Contraseña"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(e) => {setPassword(e.target.value)}}
          />
          <material.Button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
            className={clases.submit}
            onClick={signup}
          >
            Registrar
          </material.Button>
        </form>
      </div>
    </material.Container>
  )
}

