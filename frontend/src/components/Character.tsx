import React from 'react';
import * as material from '@material-ui/core';

const useStyles = material.makeStyles({
  root: {
    width: 350,
    maxWidth: '36ch',
    '& p': {
      color: '8d99ae',
    },
  },
  media: {
    height: 300,
  },
  pos: {
    marginBottom: 12,
  }
});

export default function Character({ img, name, species, status, gender }) {
  const clases = useStyles();
  return (
    <material.Card className={clases.root} variant="outlined">
      <material.CardMedia className={clases.media} image={img} title={name} />
      <material.CardContent>
        <material.Typography component="h2" variant="h5" color="secondary"> 
          {name}
        </material.Typography>
        <material.Typography className={clases.pos} color="primary" component="p">
          Species: {' '}
          {species}
          <hr />
          Status: {' '}
          {status}
          <hr />
          Gender: {' '}
          {gender}
        </material.Typography>
      </material.CardContent>
    </material.Card>
  )
}