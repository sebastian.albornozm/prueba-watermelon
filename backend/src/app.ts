import Server from './server';
import { ENV } from './config/env.config'

export const server = Server.init(parseInt(ENV.PORT));

server.start(() => {
  console.log("Server runing at port " + server.port);
});
