import express from 'express';
import userRoutes from './services/users/routes';
import repositoryRoutes from './services/search/routes'
import cors from 'cors';

export default class Server {

  public app: express.Application;
  public port: number;

  constructor(port: number) {
    this.port = port;
    this.app = express();
  }

  static init(port: number) {
    return new Server(port);
  }

  config(): void {
    this.app.use(cors());
    this.app.use(express.json());
    this.app.use("/user", userRoutes);
    this.app.use("/repository", repositoryRoutes);
  }

  start(callback: Function) {
    this.config();
    this.app.listen(this.port, callback());
  }
}
