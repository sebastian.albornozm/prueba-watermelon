import jwt from 'jsonwebtoken';
import { ENV } from '../../config/env.config';

export default class Token{

  static tokenize(username: string) {
    let jwt_token = jwt.sign({
      username: username,
    }, ENV.JWT_ENCRYPTION, {
      expiresIn: parseInt(ENV.JWT_EXPIRATION),
    });
    return jwt_token;
  }
}
