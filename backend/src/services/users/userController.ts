import { Request, Response } from 'express';
import { User } from "./user";
import Token from './../tokenize/Token';
import StoreService from '../store/StoreService';
import EncryptPassword from '../encryption/Encrypt';

export class UserController {
  public async createUser(req: Request, res: Response) {
    try {
      let user = new User(req.body.username, req.body.password);
      user = await EncryptPassword.encryptPassword(user);
      await StoreService.saveUsersInDatabase(user);
      const token = Token.tokenize(user.getUsername());
      if (token) {
        res.status(200).json({ mensaje: `Usuario ${user.getUsername()} creado `, token, kk: "kk" });
      }
    } catch (err) {
      console.log(err);
    }
  }
  
  public async login(req: Request, res: Response) {
    try {
      const user = new User(req.body.username, req.body.password);
      const matchedPassword = await StoreService.getUsersFromDatabase(user);
      if (matchedPassword != null && matchedPassword != undefined && matchedPassword === true) {
        const token = Token.tokenize(user.getUsername());
        res.status(200).json({
          token,
        });
      } else {
        res.status(401).json({
          mensaje: "Contraseña incorrecta",
        });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ msg: err });
    }
  }
}
