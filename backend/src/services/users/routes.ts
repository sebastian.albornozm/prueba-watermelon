import { Request, Response, Router } from 'express';
import { UserController } from './userController';

const controller = new UserController;
const router = Router();

router.post('/signup', controller.createUser);
router.post('/login', controller.login);

export default router;