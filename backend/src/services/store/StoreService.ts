import { redisClient } from '../../config/db.config';
import { User } from './../users/user';
import Encrypt from './../encryption/Encrypt';

export default class StoreService {
  static async saveUsersInDatabase(user: User) {
    try {
      await redisClient.set(user.getUsername(), user.getPassword());
    } catch (error) {
      throw new Error(error);
    }
  }

  static async getUsersFromDatabase(user: User) {
    try {
      let matched:any;
      await redisClient.get(user.getUsername()).then((pass) => {
        if (pass != null) {
          matched = Encrypt.comparePasswords(user.getPassword(), pass.toString());
        }        
      }).catch((err) => {
        console.log(err);
      }); 
      return matched;
    } catch (err) {
      console.log(err);
    }
  }
}