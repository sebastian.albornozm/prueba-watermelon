import { RickAndMortyRepository } from './rickandmorty';
import { Request, Response } from 'express';
import Axios from 'axios';
import { Personaje } from './personaje.interface';

export class RickAndMortyController {
  
  public async retrieveContent(req: Request, res: Response) {
    try {
      let lista = new Array<Personaje>();
      const rickandmorty = new RickAndMortyRepository;
      rickandmorty.goToPage(req.query.page);
      await Axios.get(rickandmorty.getUri(), {
        params: {
          page: rickandmorty.getPageNumber(),
        }
      }).then(function (response) {
        let data = response.data.results;
        for (let pj of data) {
          lista.push(rickandmorty.setCharacter(pj));
        }
        res.status(200).json({ personajes: lista });
      }).catch(function (error) {
        console.log("Error" + error);
        res.status(500).json({ msg: error });
      })
    } catch (err) {
      console.log("Error" + err);
    }
  }
}