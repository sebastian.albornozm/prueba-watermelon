import { Router } from 'express';
import { RickAndMortyController } from './controller';
import { authenticateJWT } from '../../middleware/auth';

const controller = new RickAndMortyController;
const router = Router();

router.get('/data', authenticateJWT, controller.retrieveContent);

export default router;