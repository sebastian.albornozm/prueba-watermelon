export interface Personaje{
  name: string,
  status: string,
  species: string,
  gender: string,
  image: string
}
