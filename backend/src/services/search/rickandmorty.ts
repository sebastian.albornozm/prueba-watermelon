import { Personaje } from './personaje.interface'

export class RickAndMortyRepository {

    private uri = 'https://rickandmortyapi.com/api/character/';
    private pageNumber = 1;

    constructor() {
        this.uri;
        this.pageNumber;
    }

    getUri() {
        return this.uri;
    }

    getPageNumber() {
        return this.pageNumber;
    }

    addPageNumber() {
        this.pageNumber += 1;
    }

    backPage() {
        this.pageNumber -= 1;
    }

    goToPage(page: any) {
        this.pageNumber = page;
    }

    setCharacter(personaje: any): Personaje {
        let newPersonaje = { name: personaje.name, status: personaje.status, species: personaje.species, gender: personaje.gender, image: personaje.image };
        return newPersonaje;
    }
}