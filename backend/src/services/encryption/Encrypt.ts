import * as bcrypt from 'bcrypt';
import { User } from '../users/user';

export default class EncryptPassword{

  static async encryptPassword(user:User) {
    await bcrypt.hash(user.getPassword(), 12).then((hash) => {
      user.setPassword(hash);
    });
    return user;
  }

  static async comparePasswords(passFromRequest: string, passFromDb:string) {
    try {
      const compared = await bcrypt.compare(passFromRequest, passFromDb);
      return compared;
    } catch (error) {
      console.log(error);
    }
  }

}