import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { ENV } from './../config/env.config'

export const authenticateJWT = (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.headers.authorization;

  if (authHeader) {
    const token = authHeader.split(" ")[1];

    jwt.verify(token, ENV.JWT_ENCRYPTION, (err, data) => {
      if (err) {
        return res.status(403).json({
          mensaje: "token fallida",
          token: data,
        })
      }
      next();
    });
  } else {
    res.sendStatus(401);
  }
}