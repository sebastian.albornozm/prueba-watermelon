import * as dotenv from 'dotenv';

dotenv.config();

export const ENV = {
  PORT: process.env.PORT || '3030',

  JWT_ENCRYPTION: process.env.JWT_ENCRYPTION || 'r1ck4ndm0rtys3rv3r',
  JWT_EXPIRATION: process.env.JWR_EXPIRATION || '120',

  DB_HOST: process.env.DB_HOST || '0.0.0.0',
  DB_PORT: process.env.DB_PORT || '6379',
}