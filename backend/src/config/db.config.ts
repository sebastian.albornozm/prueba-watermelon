import { Tedis } from 'tedis';
import { ENV } from './env.config';

export const redisClient = new Tedis({
  host: ENV.DB_HOST,
  port: parseInt(ENV.DB_PORT)
});

