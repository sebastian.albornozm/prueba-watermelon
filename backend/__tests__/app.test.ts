import { User } from './../src/services/users/user';
import { UserController } from './../src/services/users/userController';
import { Personaje } from './../src/services/search/personaje.interface';
import sinon from 'sinon';

const test_username = 'rick_sanchez';
const test_password = 'dimension_c137';
const test_required_page = '32';
const test_unexistent_page = '100';
const test_wrong_jwt = 'r&m_rocks123';
let jwt: string;

const usr_controller = new UserController;

describe('User tests', () => {

  // const mockResponse = () => {
  //   const res = {}
  //   res.status = sinon.stub().returns(res);
  //   res.json = sinon.stub().returns(res);
  //   return res;
  // }
  
  const mockRequest = (status:any, body:any) => ({
    status: status,
    body,
  });

  it('Should create a user object', () => {
    const user = new User(test_username, test_password);
    expect(user.getUsername()).toEqual(test_username);
    expect(user.getPassword()).toEqual(test_password);
  });

  it('Should create user via request and status code must be 200', async (t) => {
    const req = mockRequest({}, {
      username: test_username,
      password: test_password
    });
    // const res = mockResponse();    

  });

  it('Must succesful login, status code = 200', async (t) => {
    
  });

  it('Must login with wrong password, status code should be 401', async (t) => {
    
  });

  it('Must be an error for incorrect token, status code should be 401', async (t) => {
    
  });
});

describe('Characters test', () => {

  // const mockRequest = () => {
  //   const req = {};
  //   req.status = sinon.stub().returns(req);
  //   req.json = sinon.stub().returns(req);
  //   return req;
  // }

  //   const res = {};
  //   res.status = sinon.stub().returns(res);
  //   res.json = sinon.stub().returns(res);
  //   return res;
  // }

  it('Should request with no specific page(so it must be page=1), status code must be 200', async done => {
    done();
  });

  it('Should request especific page, status code must be 200', async done => {
    done();
  });

  it('Should request page out of bounds, status code must be ?', async done => {
    done();
  });

  it('Shold request any page with wrong jwt_token, status code must be 401', async done => {
    done();
  });

  it('Must return a list of personaje interface, assertion must be true', async done => {
    done();
  });
});